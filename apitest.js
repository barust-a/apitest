require('dotenv/config');
var express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser'); 
const userRoute = require('./api/routes/users');
const emailRoute = require('./api/routes/emails');
var app = express();


//middleware, permettent un routage plus claire dans ./api/routes/
app.use(bodyParser.json());
app.use('/users', userRoute);
app.use('/emails', emailRoute);


//connect to mongodb in the cloud
mongoose.connect(
    "mongodb+srv://root:" +
        process.env.MONGO_PASSWORD +
        "@apitestdb-ex4ak.mongodb.net/test?retryWrites=true&w=majority",
        {useNewUrlParser: true},  () =>     //useNewUrlParser to avoid deprecation message
        console.log("connected to db")
);

//server listen to port 3000
app.listen(3000);