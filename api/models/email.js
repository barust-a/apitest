const mongoose = require('mongoose');

const emailSchema = mongoose.Schema({
    from: String,
    to: String,
    object: String,
    body: String,
    date: Date
});

module.exports = mongoose.model('email', emailSchema);