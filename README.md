# apitest
apitest émerveillera petits et grands, une API pour toute la famille.

Le point d'entré est apitest.js
pour la creation (avec postman) il faut envoyer des données en raw au format json ex :
{
	"name":"lastman",
	"email": "lastman@gmail.com"
}

Pour le test j'ai exceptionellement commit le .env

ps : J'avais réfléchis à une structure de mail en bdd bien meilleur que celle que vous allez voir
mais pour des raisons de simplicité j'ai tout mis dans une seul collection sans passer par des clefs d'utilisateurs
