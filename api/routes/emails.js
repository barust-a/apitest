const Email = require('../models/email');
const express = require('express');
const router = express.Router();

//get inbox emails
router.get("/:userEmail/", (req, res) => {
    Email.find({to: req.params.userEmail})
    .then(data => {res.json(data)})
    .catch(err => {res.json({message: err})
    });
});


//get the emails sent by user
router.get("/:userEmail/sent", (req, res) => {
    Email.find({from: req.params.userEmail})
    .then(data => {res.json(data)})
    .catch(err => {res.json({message: err})
    });
});


//get emails since specific date
router.get("/:userEmail/date/:date", (req, res) => {
    const date = Date.parse(req.params.date);
    //select all email from a user (sent and inbox)
    Email.find({$or:[{from: req.params.userEmail},{to:req.params.userEmail}]})
    .where('date').gt(date)                 //filter by date
    .then(data => {res.json(data)}) 
    .catch(err => {res.json({message: err})
    });
});


//get emails from body content
router.get("/:userEmail/search/:str", (req, res) => {
    Email.find({$or:[{from: req.params.userEmail},{to:req.params.userEmail}]})
    .where({ "body" : { $regex: req.params.str, $options: 'i' } }) //filter mail to get only one with filters
    .then(data => {res.json(data)})
    .catch(err => {res.json({message: err})
    });
});

module.exports = router;