const express = require('express');
const router = express.Router();
const User =  require('../models/user');

//get all users
router.get("/", (req, res) => {
    User.find()
    .then(data => {res.json(data)})
    .catch(err => {res.json({message: err})
    });
});


//get only one user
router.get("/:userId", (req, res) => {
    User.find().where({_id: req.params.userId})
    .then(data => {res.json(data)})
    .catch(err => {res.json({message: err})
    });
});


//create a user
router.post("/create", (req, res) => {
    const user = new User({
        name: req.body.name,
        email: req.body.email
    });
    User.count({email : req.body.email}, function(err, count) { //check if a user with same email exist
        if (count == 0) {
            user.save()
            .then(data => {
                res.status(201)
                res.json(data) })
            .catch(err => {res.json({message: err})
            });
        }
        else
            res.send("email already exist");
    })
});

module.exports = router;